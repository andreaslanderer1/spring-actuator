export class LoggerDTO {

  possibleLogLevels: string[] = [];
  availableLoggers: string[] = [];
  availableLevelsPerLoggers: string[] = [];
}
