import {Component, OnInit} from '@angular/core';
import {LoggersService} from './loggers.service';
import {LoggerDTO} from './LoggerDTO';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  loggerDTO = new LoggerDTO();
  selectedLogger: string;
  selectedLogLevel: string;

  constructor(private service: LoggersService) {}

  ngOnInit(): void {
    this.getAllLoggers();
  }

  private getAllLoggers() {
    this.service.getLoggers().subscribe(result => {
      console.log(result);
      this.loggerDTO.availableLoggers = [];
      this.loggerDTO.availableLevelsPerLoggers = [];
      this.loggerDTO.possibleLogLevels = result.levels;
      for (const logger in result.loggers) {
        if (result.loggers.hasOwnProperty(logger)) {
          this.loggerDTO.availableLoggers.push(logger);
          this.loggerDTO.availableLevelsPerLoggers.push(result.loggers[logger].effectiveLevel);
          console.log(`Pushed ${logger} and ${result.loggers[logger].effectiveLevel} to the arrays!`);
        }
      }
    });
  }

  onLoggerSelected(value: string) {
    const index = this.loggerDTO.availableLoggers.indexOf(value);
    this.selectedLogger = value;
    this.onLevelSelected(this.loggerDTO.availableLevelsPerLoggers[index]);
    console.log(`Updated logger to ${this.selectedLogger}`);
  }

  onLevelSelected(value: string) {
    this.selectedLogLevel = value;
    console.log(`Updated level to ${this.selectedLogLevel}`);
  }

  onLogLevelUpdate() {
    this.service.setLogLevel(this.selectedLogger, this.selectedLogLevel).subscribe(result => {
      this.getAllLoggers();
    });
  }
}
