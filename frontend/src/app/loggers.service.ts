import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoggersService {

  private LOGGERS_URL = '/actuator/loggers';

  constructor(private httpClient: HttpClient) { }

  getLoggers(): Observable<any> {
    return this.httpClient.get(this.LOGGERS_URL)
      .pipe(
        catchError(this.handleError([]))
      );
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  setLogLevel(selectedLogger: string, selectedLogLevel: string) {
    return this.httpClient.post(`${this.LOGGERS_URL}/${selectedLogger}`, {
      configuredLevel: selectedLogLevel
    }).pipe(
        catchError(this.handleError())
    );
  }
}
